const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const api = require("./api/index");
const PORT = process.env.PORT || 3001;
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(require("./api/index"));
app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});

const {MongoClient} = require('mongodb');

async function main(){
    /**
     * Connection URI. Update <username>, <password>, and <your-cluster-url> to reflect your cluster.
     * See https://docs.mongodb.com/ecosystem/drivers/node/ for more details
     */
    const uri = "mongodb+srv://bharat408:123%40Admin@cluster0.v59wq.mongodb.net/upskill";
 
    console.log('trying to log ..... ')
    const client = new MongoClient(uri);
 
    try {
        // Connect to the MongoDB cluster
        await client.connect();
        // app.request.client = client
        app.request.myclient = client
        // const cursor = await client.db("upskill").collection("users").find().toArray()
        console.log('connected to server')
        // Make the appropriate DB calls
        // await  listDatabases(client);
 
    } catch (e) {
        console.error(e);
        console.log('mongo db error occured ... ')
    } finally {
       // await client.close();
    }
}

main().catch(console.error);

app.use("/api", api);
